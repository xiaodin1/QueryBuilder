<%@ page language="java" import="java.util.*,java.io.*" pageEncoding="utf-8"%>
<%@page language="java" import="java.util.Date" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>QueryBuilder-信息统计</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta name="baidu_union_verify" content="57500a7ebde840d31e0693088442c5e8">
<meta http-equiv="description" content="This is my page">
<meta name="keywords" content="QueryBuilder,Jquery,Bootstrap,jquery,queryBuilder,高级自定义查询,可视化SQL拼接,优雅的可视化SQL拼接,复杂SQL拼接,queryBuilder-jquery"/>
<meta name="description" content="QueryBuilder,Jquery,Bootstrap,jquery,queryBuilder,高级自定义查询,可视化SQL拼接,优雅的可视化SQL拼接,复杂SQL拼接,queryBuilder-jquery" />
<link rel="shortcut icon" href="<%=path %>/css/images/ico.ico" type="image/x-icon" /> 

<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet"
	href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap.min.css">

<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet"
	href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="<%=path %>/css/base.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="<%=path %>/js/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="<%=path%>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

</head>

<body>
	<div class="jumbotron" >
	总共访问者数量为:<span>${count }</span>
	<table width="100%">
		<c:forEach var="ip" items="${ips }" varStatus="varStatusName">
		<tr>
			<td>${ip.ip }</td><td>${ip.area }</td><td>${ip.visitDate }</td>
		</tr>
	</c:forEach>
	
	</table>
	</div>
</body>
</html>
