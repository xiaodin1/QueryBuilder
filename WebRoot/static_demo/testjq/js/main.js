/**
 * Created by huipu on 2016/1/28.
 *	分页插件
 */
var kkpager = {
    //divID
    parentId: 'div_pager',
    //保存的结构
    saveData: 1,
    //元数据
    sourceData: 1,
    //回调函数
    callbackFunction:function(key,connect){},
    //初始化组件
    init: function (config) {
        //页面条数初始化
        this.parentId = config.parentId;
        this.saveData = config.saveData;
        this.sourceData = config.sourceData;
        if(config.sourceData == undefined){
            this.inited = false;
        }
        this.inited = true;
        this.generPageHtml();
    },
    //生成分页控件Html
    generPageHtml: function () {
        if (!this.inited) {
            return;
        }
        var a = "userId",b = "==";
        $('#'+this.parentId).append('<a href="javascript:void(0)">测试</a>');
        $('#'+this.parentId).find('a').on('click',function(){
            this.callbackFunction(a,b);

        });
    }
};

function getParameter(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
};
